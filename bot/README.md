[[_TOC_]]

## Katana RS.

Welcome to the offical Gitlab page of Katana RS. This is a bot I plan to expand to be a bot with many utilites!
This bot was made with asynchronous [serenity.rs](https://github.com/Lakelezz/serenity/await)
If you would like the invite, [click here](https://discordapp.com/developers/applications/658056989347414026/oauth2), you may add the permissions it needs (aka for basic moderation and such)

## Help

If you need help with the bot, or would like to contact the owner, heres the offical [Discord Support Server](https://discord.gg/NcYKBUZ) for Katana RS.
This server also has a help category in case you are looking for help related to the bot/code

## Credits/Miscellaneous

I would like to thank the Serenity & Rustlang community for there help. There [Discord Server](https://discord.gg/WBdGJCc) and Github(https://github.com/Lakelezz/serenity/) can be found by clicking the buttons

I would also like to thank the special person which introduced me and guided me to make this bot in Rustlang, nitsuga5124. His [Discord](https://discord.gg/ZNKecRV), [Gitlab](https://gitlab.com/nitsuga5124) and [Discord Bot](https://discord.com/oauth2/authorize?client_id=551759974905151548&scope=bot&permissions=825748694) can be found by clicking the words.

Else, thanks for coming here and have a good day!
