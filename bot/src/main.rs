use std::env;
use std::sync::Arc;
use std::collections::{HashMap, HashSet};

/* Ah yes, also add serenity into your Cargo.toml file. You can find an example in my Cargo.toml file */
use serenity::{
    client::bridge::voice::ClientVoiceManager,
    async_trait,
    framework::standard::{
        Args, CheckResult, CommandOptions, CommandResult, CommandGroup,
        DispatchError, HelpOptions, help_commands, StandardFramework,
        macros::{command, group, help, hook}
    },
    http::Http,
    model::{
       channel::{Channel, Message}, gateway::Ready, id::UserId,
       permissions::Permissions, id::ChannelId, id::MessageId, channel::GuildChannel,
   },
   Result as SerenityResult,
};

use serenity::prelude::*;
use serenity_lavalink::{
    LavalinkClient,
    nodes::Node,
};

/* This part is only necessary [below] if you have any cogs/modules inside another subfolder.
    Make sure you make a mod.rs though, which holds the other modules*/
mod commands;

use commands::{
    moderation::*,
    fun::*,
    music::*,
    tags::*,
    database_stuff::*,
};

use sqlx::PgPool; //import to use with postgres
use std::convert::TryInto;

struct DatabasePool; //To make a single connection!

struct VoiceManager; //Struct our Voicemamnager

struct Lavalink; //Struct our Lavalink client

//implement TypeMapKey for our VoiceManager with a type "Value" being the arc
impl TypeMapKey for VoiceManager {
    type Value = Arc<Mutex<ClientVoiceManager>>;
}

//Ah yes, making a LavalinkClient;
impl TypeMapKey for Lavalink {
    type Value = Arc<RwLock<LavalinkClient>>;
}

impl TypeMapKey for DatabasePool {
    type Value = PgPool;
}

struct Handler; //struct for EventHandler's like Ready, MessageDelete, MessageCreate, etc.,

/* The structs below define whatever "groups" we want our commands to be in. Keep in mind, the names and commands in them are totally optional
    If you want to just make one struct, its up to you, but these really help decifer command groups in the built-in help command
    Notice we have put the group macro to idenify this struct as a group, and not just a struct in our code
    Also, the commands macro allows you to put any command inside it, like shown, so yeah bare with me.*/

#[group]
#[commands("echo", "cc", "cr", "dr", "serverinfo", "logs", "prefix")]
struct Moderationstuff;

#[group]
#[commands("_8ball", "rng", "colour", "make_colour")]
struct Funstuff;

#[group]
#[commands("join", "play", "leave", "pause", "resume", "jump", "volume")] //Lavalink ahh yes
struct Music;

#[group]
#[commands("tag")]
struct Tags;

#[group]
#[commands("rank", "disable", "enable", "leaderboard", "xprate")]
struct Leveling;
/* We put this async_trait macro to signify this as an async trait in our command
    Also, we use the 'impl' keyword to implement the EventHandler for the struct Handler
    You can find help with implements at: https://doc.rust-lang.org/std/keyword.impl.html*/

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, _: Context, ready: Ready) {
        println!("{} is ready!", ready.user.name);
    }

    async fn message(&self, ctx: Context, msg: Message) {
        if msg.author.bot { //Check if the author is a bot
            return; //return mean dont do anything
        }

        let data = ctx.data.read().await; //read the data from context (its a RwLock)
        let pool = data.get::<DatabasePool>().unwrap(); //get the DBPool
        let guild = msg.guild_id.unwrap(); //Also unwrap the message guild

        let bool_to_seek = sqlx::query!("SELECT status FROM levels WHERE guild = $1 AND author = $2", guild.0 as i64, msg.author.id.0 as i64).fetch_optional(pool).await.unwrap();

        if let Some(stat) = bool_to_seek {
            let statu = stat.status.unwrap();
            if statu == false {
                return;
            }
        }

        let xprate = sqlx::query!("SELECT xp_rate FROM guild_config WHERE guild = $1", guild.0 as i64).fetch_optional(pool).await.unwrap();

        if let Some(xprate) = xprate {
        } else {
            sqlx::query!("INSERT INTO guild_config(guild, logs) VALUES ($1, $2)", guild.0 as i64, 0).execute(pool).await.unwrap();
        }


        let user = sqlx::query!("SELECT author FROM levels WHERE guild = $1 AND author = $2", guild.0 as i64, msg.author.id.0 as i64).fetch_optional(pool).await.unwrap();

        if let Some(user) = user {
            let xprate = sqlx::query!("SELECT xp_rate FROM guild_config WHERE guild = $1", guild.0 as i64).fetch_optional(pool).await.unwrap();
            let curr_xp = sqlx::query!("SELECT xp FROM levels WHERE guild = $1 AND author = $2", guild.0 as i64, msg.author.id.0 as i64).fetch_optional(pool).await.unwrap();
            let level = sqlx::query!("SELECT lvl FROM levels WHERE guild = $1 AND author = $2", guild.0 as i64, msg.author.id.0 as i64).fetch_optional(pool).await.unwrap();
            let xpr = xprate.unwrap().xp_rate.unwrap();
            let current_xp = curr_xp.unwrap().xp.unwrap();
            let new_xp = current_xp + 2 * xpr;

            sqlx::query!("UPDATE levels SET xp = $1 WHERE author = $2", new_xp, msg.author.id.0 as i64).execute(pool).await.unwrap();

            let current_level = level.unwrap().lvl.unwrap();
            let equation = current_level * 3;
            let other_eq = equation * equation;

            if current_xp > other_eq {
                let new_lvl = current_level + 1;
                msg.channel_id.say(&ctx, format!("{} has leveled up to {}!", msg.author.name, new_lvl)).await.ok();
                sqlx::query!("UPDATE levels SET lvl = $1 WHERE author = $2", new_lvl, msg.author.id.0 as i64).execute(pool).await.unwrap();
            }
        } else {
            sqlx::query!("INSERT INTO levels(author, guild, lvl, xp, status) VALUES ($1, $2, $3, $4, $5)", msg.author.id.0 as i64, guild.0 as i64, 1, 0, true).execute(pool).await.unwrap();
        }
    }

    /* async fn message_delete(&self, ctx: Context, chan: ChannelId, msg: MessageId) {
        let data = ctx.data.read().await; //read the data from context (its a RwLock)
        let pool = data.get::<DatabasePool>().unwrap(); //get the DBPool
        let guild = msg.guild_id.unwrap(); //Also unwrap the message guild

        let channel = sqlx::query!("SELECT logs FROM guild_config WHERE guild = $1", guild.0 as i64).fetch_optional(pool).await.unwrap();

        let channel_send = channel.unwrap().logs.unwrap();
        let chan = ctx.http.as_ref().get_channel(channel_send as u64).await;

        let new_chan = match chan {
            Ok(chan) => chan,
            Err(_) => return,
        };

        let log = match new_chan.guild() {
            Some(log) => log,
            None => return,
        };

        log.say(&ctx, "Someone delted message lol!").await.ok(); 
    } */

    /* async fn channel_create(&self, ctx: Context, channel: Arc<RwLock<GuildChannel>>) {
        let data = ctx.data.read().await; //read the data from context (its a RwLock)
        let pool = data.get::<DatabasePool>().unwrap(); //get the DBPool

        let channel = sqlx::query!("SELECT logs FROM guild_config WHERE guild = $1", guild.0 as i64).fetch_optional(pool).await.unwrap();

        let channel_send = channel.unwrap().logs.unwrap();
        let chan = ctx.http.as_ref().get_channel(channel_send as u64).await;

        let new_chan = match chan {
            Ok(chan) => chan,
            Err(_) => return,
        };

        let log = match new_chan.guild() {
            Some(log) => log,
            None => return,
        };

        log.say(&ctx, format!("{} channel was created!", channel.read().name)).await.ok(); 

    } */
}

#[hook]
async fn dynamic_prefix(ctx: &Context, msg: &Message) -> Option<String> {
    let guild = msg.guild_id;

    if let Some(id) = guild {
        let data = ctx.data.read().await;
        let pool = data.get::<DatabasePool>()?;

        let db_prefix = sqlx::query!("SELECT prefix FROM guild_config WHERE guild = $1", guild.0 as i64).fetch_optional(pool).await?;

        let prefix = db_prefix.unwrap().prefix.unwrap();
        Some(prefix);
    }

}

//thanks to nitsu, i kinda didnt know how to get the pool info
//dyn is dynamic, and we are returning a dynamic box containing a standard error
pub async fn get_pool() -> Result<PgPool, Box<dyn std::error::Error>> {
    kankyo::load().expect("Failed to load .env"); //load the .env file

    env_logger::init(); //intiate the init logger

    let db_url = env::var("DATABASE_URL").expect("Expected a url!"); //Get the database url

    let pool = PgPool::builder() //build the pool
        .max_size(20)
        .build(&db_url)
        .await?;

    Ok(pool)
}

/* We have all these macros to help us down below with our help command. Basically it is a function that takes all the parameters and formats it into a good looking help embed command
For more information about the macros/help command, refer to: https://github.com/Lakelezz/serenity/blob/await/examples/05_command_framework/src/main.rs*/

#[help]
#[individual_command_tip="Hello! If you would like help with a certain command, just pass it as an argument!"]
#[command_not_found_text="Could not find the command: `{}`"]
#[max_levenshtein_distance(3)]
#[lacking_permissions="Hide"]
#[lacking_role="Nothing"]
#[wrong_channel="Strike"]
async fn my_help(
    context: &Context,
    msg: &Message,
    args: Args,
    help_options: &'static HelpOptions,
    groups: &[&'static CommandGroup],
    owners: HashSet<UserId>
) -> CommandResult {
    help_commands::with_embeds(context, msg, args, help_options, groups, owners).await
}

//This is our main finction, which to let tokio now, we add the #[tokio::main] macro
#[tokio::main]
async fn main() {
    kankyo::load().expect("Failed to load env file!"); //load the env file with kankyo [again, in Cargo.toml under dependencies]

    let token = env::var("DISCORD_TOKEN").expect("Expected a token in the enviorment variables!"); //In the .env its stored as DISCORD_TOKEN=mytoken

    let http = Http::new_with_token(&token); //initialize an instance with our token

    let bot_id = match http.get_current_application_info().await { //get application info of our bot
        Ok(info) => info.id, //assigning to info inside this scope
        Err(why) => panic!("Could not access application info: {:?}", why), //panic with panic!
    };

    //intiate a new command framework (the custom one, because Message is yucky) Again, these are simple configurations, but to see all of them, refer to: https://github.com/Lakelezz/serenity/blob/await/examples/05_command_framework/src/main.rs
    let framework = StandardFramework::new()
        .configure(|c| c
            .with_whitespace(true)
            .prefix("k!"))
        .help(&MY_HELP)
        .group(&MODERATIONSTUFF_GROUP)
        .group(&FUNSTUFF_GROUP)
        .group(&MUSIC_GROUP)
        .group(&TAGS_GROUP)
        .group(&LEVELING_GROUP);

    //Make a new client with our EventHandler and framework
    let mut client = Client::new(&token)
        .event_handler(Handler)
        .framework(framework)
        .await
        .expect("Error creating client");

        {
            let mut data = client.data.write().await; //making a mutable data variable so the client can write in it
            data.insert::<VoiceManager>(Arc::clone(&client.voice_manager)); //inserting data into the VoiceManager and cloning with Arc

            let pool = get_pool().await.unwrap();
            data.insert::<DatabasePool>(pool.clone()); //clone and insert the pool

            let mut lava_client = LavalinkClient::new(); //Making a new Lavalink Client (we need one lol)
            lava_client.bot_id = bot_id;
            lava_client.port = 8000; //port where the server is in
            lava_client.password = env::var("LAVALINK_PASSWORD").expect("Expected LL password"); //Do not share your password, as people can connect to your server and do some stuff on it
            lava_client.initialize().await.ok(); //initializing the lavalink client
            data.insert::<Lavalink>(Arc::new(RwLock::new(lava_client))); //Inserting data to our Lavalink struct
        }

        /* Start our client with client.start(), kind of simiar to discord.py. Also you might be wondering why eprintln! is used instead of
        println!. Its because something to do with error handling and println! making more fuss, so yeah I recommend it*/
    if let Err(why) = client.start().await {
        eprintln!("Error starting client: {:?}", why);
    }
}

    /* #[command] is always needed as a macro on each command to make it one lol.
    As you can see we call our command reply, we add Context as a parameter and Message as one (its the message where the command is invoked)*/
#[command]
async fn reply(ctx: &Context, msg: &Message) -> CommandResult {
    //Error checking is vital here. We make an if statement that if Result<> returns Err() whilst sending the message, to print what was wrong/what we assigned Err() to
    if let Err(why) = msg.channel_id.say(ctx, "Hi.").await {
        eprintln!("Error regarding reply command | {:?}", why)
    }

    Ok(()) //ALWAYS RETURN Ok(()). It expects this, if not it throws a box error
}

/* I would like to explain, that if you have 0 knowlegde of Rust, maybe this isnt the best project for you atm
Ofc, there is a lot of basic rust knowlegde here, but its kind of not that easy in a sense, so please LEARN RUST! */
