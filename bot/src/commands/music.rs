use serenity::{
    client::bridge::voice::ClientVoiceManager,
    async_trait,
    framework::standard::{
        Args, CheckResult, CommandOptions, CommandResult, CommandGroup,
        DispatchError, HelpOptions, help_commands, StandardFramework,
        macros::{command, group}
    },
    http::Http,
    model::{
       channel::{Channel, Message}, gateway::Ready, id::UserId,
       permissions::Permissions,
   },
   Result as SerenityResult,
};

use std::time::Duration;
use std::sync::Arc;
use serenity_lavalink::LavalinkClient;
use serenity_lavalink::nodes::Node;
use serenity::prelude::*;
use crate::VoiceManager;
use crate::Lavalink;

#[command]
#[only_in("guilds")]
async fn join(ctx: &Context, msg: &Message) -> CommandResult {
    let guild = match msg.guild(&ctx.cache).await {
        Some(guild) => guild,
        None => {
            msg.channel_id.say(&ctx.http, "DMs not supported").await.ok();

            return Ok(());

        },
    };

    let guild_id = guild.id; //getting the guild id by reading it and awaiting its id since its RwLock

    let channel_id = guild.voice_states.get(&msg.author.id).and_then(|v| v.channel_id); //A lot goes on, we read, then we await and check if the author is in a VC, and after seeing what VC

    let connection = match channel_id { //checking if theyre in a channel!
        Some(channel) => channel,
        None => {
            msg.channel_id.say(&ctx.http, "You are not in a VC!").await.ok();

            return Ok(());
        },
    };

    let ml = ctx.data.read().await.
        get::<VoiceManager>().cloned().expect("VoiceManager In TypeMap expected!"); //get the VoiceManager and clone it

    let mut manager = ml.lock().await; //locking the lock lol

    if manager.join(guild_id, connection).is_some() { //checking if the connection is some, meaning that if it is in a VC
        let lava_client_lock = ctx.data.read().await;
        let e = lava_client_lock.get::<Lavalink>().expect("Expected Lava as TypeMap form");
        let mut lava_client = e.write().await; //Since this is a Rwlock, we can write stuff
        Node::new(&mut lava_client, guild_id, msg.channel_id); //initate a node for the guild
        msg.channel_id.say(ctx, format!("I have joined {}!", connection.mention())).await?;
    } else { //else an error occurs
        msg.channel_id.say(&ctx, "Error joining the VC.").await.ok();
    }

    Ok(())
}

#[command]
#[min_args(1)]
#[only_in("guilds")]
async fn play(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let query = args.message().to_string();  //emptying the strings to make it clear to the searcher of this wrapper

    let ml = ctx.data.read().await //read through the lock data/voice data
        .get::<VoiceManager>().cloned().expect("VoiceManafger In TypeMap expected"); //expected the implementation of the TypeMapKey

    let mut manager = ml.lock().await; //again, locking what needs to be locked

    if let Some(handler) = manager.get_mut(&msg.guild_id.unwrap()) { //Naking some handler be the manager getting the mutable guild id
        let data = ctx.data.read().await; //reading data again
        let lava_lock = data.get::<Lavalink>().expect("Expected Lavalink in TypeMap"); //gets the Lavalink Client of that guild
        let mut lc = lava_lock.write().await; //Write on the lock

        let track = lc.auto_search_tracks(&query).await?; //Making the wrapper search for us in YT (thank you nitsuga)

        if track.tracks.is_empty() { //Since it returns a vector, we can check if its empty with the method .is_empty()
            msg.channel_id.say(&ctx, "Could not find any video of the search query.").await.ok();
            return Ok(());
        }

        {
            let node = lc.nodes.get_mut(&msg.guild_id.unwrap()).unwrap(); //Get all the node/nodes for the guild (can have multiple nodes so this makes my lavalink python bot look bad)

            node.play(track.tracks[0].clone()).queue(); //Make the node play the song and queue
        }

        let node = lc.nodes.get(&msg.guild_id.unwrap()).unwrap();

        if !lc.loops.contains(&msg.guild_id.unwrap()) {
            node.start_loop(Arc::clone(lava_lock), Arc::new(handler.clone())).await; //Start a node/node loop!
        }

        msg.channel_id.say(&ctx.http, format!("I have enqueued {}", track.tracks[0].info.title)).await.ok();//Sat we are playing the track

    } else {
        msg.channel_id.say(&ctx.http, "Use `k!join` first, to connect the bot to your current voice channel.").await.ok(); //else we arent in A VC
    }

    Ok(())
}

#[command]
#[only_in("guilds")]
async fn leave(ctx: &Context, msg: &Message) -> CommandResult {
    let ml = ctx.data.read().await
        .get::<VoiceManager>().cloned().expect("Expected VoiceManager in TypeMap form"); //read the Voicemanager as we do for join and play

    let mut manager = ml.lock().await;
    let has_handler = manager.get(&msg.guild_id.unwrap()).is_some(); //GEt the VoiceManager of the guild id and check if its any

    if has_handler { //checking if its some
        manager.remove(&msg.guild_id.unwrap());
        {
            let data = ctx.data.read().await;
            let lava_lock = data.get::<Lavalink>().cloned().expect("Expected Lavalink in TypeMap form!"); //Read and get the Lavalink client
            let mut lc = lava_lock.write().await;
            let node = lc.nodes.get(&msg.guild_id.unwrap()).unwrap().clone(); //Clone and unwrap a node

            node.destroy(&mut lc, &msg.guild_id.unwrap()).await?; //Destroy the noob node
        }

        msg.channel_id.say(&ctx, "Left the Voice Channel!").await.ok(); //Leave the channel and say something
    } else {
        msg.channel_id.say(&ctx, "Not connected to a Voice Channel").await.ok(); //Else the node isnt even connected
    }

    Ok(())
}

#[command]
#[only_in("guilds")]
async fn pause(ctx: &Context, msg: &Message) -> CommandResult {
    let ml = ctx.data.read().await.get::<VoiceManager>().cloned().expect("Expected VoiceManager in TypeMap form"); //One of the good things of Rust is you can do methods downwards like in the leave command or like This

    let manager = ml.lock().await;
    let has_handler = manager.get(&msg.guild_id.unwrap()).is_some(); //See if the guild has a VoiceManager/Client

    if has_handler { //if it does, for learning if/else statements refer to this: https://doc.rust-lang.org/stable/rust-by-example/flow_control/if_else.html
        let data = ctx.data.read().await;
        let lava_client_lock = data.get::<Lavalink>().cloned().expect("Expected Lavalink in TypeMap form"); //Get its data
        let mut lava_client = lava_client_lock.write().await;
        let mut node = lava_client.nodes.get(&msg.guild_id.unwrap()).unwrap().clone();

        node.pause(&mut lava_client, &msg.guild_id.unwrap()).await?;

        msg.channel_id.say(ctx, "I have paused the music!").await.ok(); //You can use '?' operator but I use .ok() to ensure its ok
    } else {
        msg.channel_id.say(ctx, "Im either not connected to a VC or something went wrong.").await.ok();
    }

    Ok(())
}

#[command]
#[only_in("guilds")]
async fn resume(ctx: &Context, msg: &Message) -> CommandResult {

    let ml = ctx.data.read().await.get::<VoiceManager>().cloned().expect("Expected VM in TypeMap form"); //again checking for typemap form
    let manager = ml.lock().await;
    let has_handler = manager.get(&msg.guild_id.unwrap()).is_some(); //GEt it with the is_sone to not check for it in the if statement

    if has_handler {
        let data = ctx.data.read().await; //Get data through the context and await it
        let lava_lock = data.get::<Lavalink>().cloned().expect("Expected lavalink in Typemap form"); //Checks do be lifesaving
        let mut kava_client = lava_lock.write().await; //Again '?' like .ok() is helpful
        let mut node = kava_client.nodes.get(&msg.guild_id.unwrap()).unwrap().clone(); //Check the guild node

        node.resume(&mut kava_client, &msg.guild_id.unwrap()).await?; //Resume the node with the lava client

        msg.channel_id.say(ctx, "I have resumed the music!").await.ok();
    } else {
        msg.channel_id.say(ctx, "Im not connected to a voice channel.").await.ok(); //If this triggers it means what youre reading
    }

    Ok(())
}

#[command]
#[only_in("guilds")]
#[min_args(1)]
async fn jump(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let mil = args.single::<u64>()?; //Grab the first argument as 64 as below is needed
    let secs = mil * 1000; //Multiply 1000 to make it seconds
    let milis = Duration::from_millis(secs); //turn it into duration/time as required

    let ml = ctx.data.read().await.get::<VoiceManager>().cloned().expect("Expected VM in Typemap Form"); //checks checks checks
    let manager = ml.lock().await;
    let has_handler = manager.get(&msg.guild_id.unwrap()).is_some();

    if has_handler {
        let lava_client_lock = ctx.data.read().await.get::<Lavalink>().cloned().expect("Expected impl for Lavalink"); //One of the great things again is how versatile rust is with methods
        let mut lock_read = lava_client_lock.read().await.clone(); //Clone and read the Rwlock of the lava client lock
        let lc = lava_client_lock.write().await; //Write on the lock
        let mut node = lc.nodes.get(&msg.guild_id.unwrap()).unwrap().clone(); //Guild nodes

        node.seek(&mut lock_read, &msg.guild_id.unwrap(), milis).await?; //Seek to the time

        msg.channel_id.say(ctx, format!("I have gone to second `{:?}` of the song!", mil)).await.ok();
    } else {
        msg.channel_id.say(ctx, "I am not connected to a VC!").await.ok();
    }

    Ok(())
}

#[command]
#[min_args(1)]
#[only_in("guilds")]
async fn volume(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let dvol = args.single::<u16>()?;

    let ml = ctx.data.read().await.get::<VoiceManager>().cloned().expect("Expected VM in TypeMap form"); //again checking for typemap form
    let manager = ml.lock().await;
    let has_handler = manager.get(&msg.guild_id.unwrap()).is_some(); //GEt it with the is_sone to not check for it in the if statement

    if has_handler {
        let data = ctx.data.read().await; //Get data through the context and await it
        let lava_client_lock = data.get::<Lavalink>().cloned().expect("Expected lavalink in Typemap form"); //Checks do be lifesaving
        let mut lava_client = lava_client_lock.write().await; //Writeeee
        let mut node = lava_client.nodes.get(&msg.guild_id.unwrap()).unwrap().clone();

        node.set_volume(&mut lava_client, &msg.guild_id.unwrap(), dvol).await?; //Set the nodes volume

        msg.channel_id.say(ctx, format!("I have set the volume to {}", dvol)).await.ok();
    } else {
        msg.channel_id.say(ctx, "Im not connected to a voice channel.").await.ok(); //If this triggers it means what youre reading
    }

    Ok(())
}
