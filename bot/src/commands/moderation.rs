use serenity::{
    async_trait,
    utils::*,
    framework::standard::{
        Args, CheckResult, CommandOptions, CommandResult, CommandGroup,
        DispatchError, StandardFramework, CommandError,
        macros::{command, group} //lazy so just imported what the main.rs file had
    },
    http::Http,
    model::{
       channel::{Channel, Message}, gateway::Ready, id::UserId,
       permissions::Permissions, guild::Guild, guild::Role, guild::Member, id::ChannelId,
   },
};

use serenity::prelude::*;
use serenity::model::*;
use serenity::model::channel::ChannelType;

use crate::DatabasePool;

use sqlx;


pub async fn get_role(ctx: &Context, msg: &Message, role: &str) -> Option<Role> { //Here we require a role as a string and also the other fields
    return ctx.cache.guild_field(msg.guild_id?, |g| g.role_by_name(role).cloned()).await.unwrap();
}

/* pub async fn get_channel(ctx: &Context, msg: &Message, channel: &str) -> Option<Channel> {
    return ctx.cache.guild_field(msg.guild_id?, |g| g.channel_id_by_name(&ctx.cache, channel).clone()).await.unwrap();
} */

//Command to echo everything I want it to lol like i.e: k!echo Bruh
#[command]
async fn echo(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let things = args.message(); //get all the args that are needed, aka all the args which were invoked with the command/message

    if let Err(why) = msg.channel_id.say(ctx, &things).await {
        eprintln!("Error regarding echo | {:?}", why)
    }

    Ok(()) //<- remember, youre gonna see that a lot
}

/* Ahhh yes, aliases and required_permissions macros. Let me explain, but you can see them all at https://docs.rs/serenity/0.8.6/serenity/framework/standard/macros/index.html
    Brief summary:
    #[required_permissions(PERMISSIONS_NEEDED)] <- tells what permissions are needed for this command, also useful for the help command formatting
    #[aliases("name1", "name2")] <- tells the framework what aliases this command has*/
#[command]
#[required_permissions(MANAGE_CHANNELS)]
#[aliases("createChannel", "create_channel", "createchannel")]
async fn cc(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let name = args.message();
    let guild = match msg.guild_id { //match statements are pog, and since this returns Option<serenity::Message::GuildId> its either some guild or none. Examples: https://doc.rust-lang.org/stable/rust-by-example/flow_control/match.html
        Some(g) => {
            g.create_channel(&ctx.http, |c| c.name(&name).kind(ChannelType::Text)).await.ok(); //.ok(); at the end to supress the error handling error lol
            msg.channel_id.say(ctx, format!("I have created the channel: {}", name)).await.ok();
        },
        None => return Err(CommandError("Couldnt register this guild, because it most likely isnt one!".to_string())), //return it so itll not trigger some future error (its async cmon)
    };

    Ok(())
}

#[command]
#[required_permissions(MANAGE_ROLES)]
#[aliases("createRole, createrole, create_role")]
async fn cr(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let name = args.message();
    match msg.guild_id { //again
        Some(g) => {
            g.create_role(&ctx.http, |r| r.hoist(true).name(&name)).await.ok();
            msg.channel_id.say(ctx, format!("I have created the role: {}", name)).await.ok();
        },
        None => {return Err(CommandError("Couldnt find a guild to create a role!".to_string()))} //.to_string() is needed since it will turn it to string from string?? idk
    };

    Ok(())
}

#[command]
#[aliases("deleterole")]
async fn dr(mut ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let role_name = args.message(); //parsing the first arg

    let role = get_role(&mut ctx, &msg, &role_name).await; //Make the function get it

    match role {
        Some(mut r) => {
            r.delete(&ctx); //Delete the role if its found

            msg.channel_id.say(ctx, format!("I have deleted the role {}", &role_name)).await?; //Send it
        },
        None => {
            msg.channel_id.say(ctx, format!("I couldnt find any role with the name of {}", &role_name)).await?;
        },
    };

    Ok(())
}

#[command]
#[aliases("si")]
async fn serverinfo(ctx: &Context, msg: &Message) -> CommandResult {
    let colour = Colour::from_rgb(255, 0, 0); //make a vibrant colour with serenity::utils::Colour
    let guild = match msg.guild(&ctx.cache).await { //match the guild where the message was sent
        Some(guild) => guild, //if there is some then yea
        None => {
            msg.channel_id.say(ctx, "I cant find info about a guild in DMS!").await?; //if theyre isnt any, just send the message and use the ? operator

            return Ok(()); //return with the Ok result
        },
    }; //end the match statement

    let mc = &guild.member_count; //We have to define the guilds info before because it has the future trait
    let role_count = &guild.roles.len(); //len() method to get the length of this vector
    let name = &guild.name;
    let emogi = &guild.emojis.len(); //again, hashmap length
    let chann = &guild.channels.len();
    let largs = &guild.large; //Large means if it has 250+ members
    let nitro_lvl = &guild.premium_tier; //check the nitro level
    let nitro_boosters = &guild.premium_subscription_count; //check how many nitro boosters
    let mfa = &guild.mfa_level; //factor authorization level

    if let Err(why) = msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e|{
            e.title("Guild Info");
            e.description(format!("Information about {}", &name));
            e.color(colour);
            e.field("Members", &mc, true);
            e.field("Roles", &role_count, true);
            e.field("Emojis", &emogi, true);
            e.field("Channels", &chann, true);
            e.field("Large? (250+)", &largs, true);
            e.field("Nitro Boost Level", format!("{:?}", &nitro_lvl), true);
            e.field("Nitro Boosters", format!("{:?}", &nitro_boosters), true);
            e.field("MFA Level", format!("{:?}", &mfa), true);
            e
        });
        m
    }).await {
        eprintln!("Error regarding si | {:?}", why);
    }

    Ok(())
}

#[command]
async fn logs(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let channelid = args.single::<ChannelId>()?;
    let guild = msg.guild_id.unwrap();
    let data = ctx.data.read().await; //we read the data
    let pool = data.get::<DatabasePool>().unwrap(); //we then get the database pool

    sqlx::query!("UPDATE guild_config SET logs = $1 WHERE guild = $2", channelid.0 as i64, guild.0 as i64).execute(pool).await?;

    msg.channel_id.say(ctx, format!("I have set the log channel to {}!", channelid.mention())).await?;

    Ok(())
}