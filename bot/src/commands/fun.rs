use serenity::{
    async_trait,
    utils::*,
    framework::standard::{
        Args, CheckResult, CommandOptions, CommandResult, CommandGroup,
        DispatchError, StandardFramework,
        macros::{command, group}
    },
    prelude::*,
    model::{
        channel::{Channel, Message}
    }
};
use serenity::http::Http;

use rand::Rng;

//Usage: k!8ball Whats after me?
#[command]
#[aliases("8ball")]
async fn _8ball(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let question = args.message();
    let choices = [
    "Seems most likely",
    "To be honest, that seems so!",
    "It is certain",
    "It is uncertain, please ask again",
    "Please, ask again",
    "It is not going to happen",
    "It is not likely to happen",
    "I cannot predict if it will or will not happen"]; //ahh yes arrays in Rust, no need to tell their size. Just make one like this

    let rand = rand::thread_rng().gen_range(0, choices.len()+1); //You may be asking, why this when you can shift it and rand.gen_range(). Well that pisses the compiler, not safe, and gives more life to the thread
    let thing = &choices[rand]; //Yep slicing. With the random number.

    //Embed as you can see below. No need for the http Context, but I always have it. Also, rust lambdas like |m| and |e| are there. They are pretty pog.
    if let Err(why) = msg.channel_id.send_message(&ctx.http, |m|{
        m.embed(|e|{
            e.title("8ball");
            e.description("The magic 8ball has responded to your call");
            e.field("Question", &question, false);
            e.field("Answer", &thing, false);
            e
        });
        m
    }).await {
        eprintln!("Error regarding 8ball | {:?}", why);
    }

    Ok(())
}

//im tired of commentating but **someone** forced me

//#[min_args(int)] is basically saying that this command needs x amount of args at least
//Usage: k!rng 69 420
#[command]
#[min_args(2)]
async fn rng(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let numone = args.single::<u64>()?; //parsing and getting a single arg and also using the ? operator as error handling, and to shut up the compiler
    let numtwo = args.single::<u64>()?;
    let range = rand::thread_rng().gen_range(numone, numtwo); //Generating a range and picking a number. Note: rand is also an extern crate, aka you need it in your Cargo.toml.

    if let Err(why) = msg.channel_id.say(ctx, format!("The number you have gotten from {} - {} is {}", numone, numtwo, range)).await {
        eprintln!("Error regarding rng command | {:?}", why) //eprintln!
    }

    Ok(())
}

#[command]
async fn colour(ctx: &Context, msg: &Message) -> CommandResult {
    let numone = rand::thread_rng().gen_range(0, 255); //uhh I can do a for loop but im commentating lol..
    let numtwo = rand::thread_rng().gen_range(0, 255);
    let numthree = rand::thread_rng().gen_range(0, 255);
    let color = Colour::from_rgb(numone, numtwo, numthree); //Yes, making a colour from the r g b values

    if let Err(why) = msg.channel_id.send_message(&ctx.http, |m|{
        m.embed(|e|{
            e.title("Random Colour");
            e.description(format!("I have made your random colour! | ({}, {}, {})", numone, numtwo, numthree));
            e.colour(color);
            e
        });
        m
    }).await {
        eprintln!("Error regarding colour command | {:?}", why);
    }

    Ok(())
}

#[command]
async fn make_colour(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let val1 = args.single::<u8>()?; //<> indicator and :: are basically pointing to enums. In this case we are pointing to the u8 inside the args enum I believe
    let val2 = args.single::<u8>()?;
    let val3 = args.single::<u8>()?;
    let wish_color = Colour::from_rgb(val1, val2, val3); //Making an rgb

    if let Err(why) = msg.channel_id.send_message(&ctx.http, |m|{
        m.embed(|e|{
            e.title("Requested Colour");
            e.description(format!("I have made your wished colour | ({}, {}, {})", val1, val2, val3));
            e.colour(wish_color);
            e
        });
        m
    }).await {
        eprintln!("Error regarding make_colour | {:?}", why);
    }

    Ok(())
}
