use serenity::{
    async_trait,
    utils::*,
    framework::standard::{
        Args, CheckResult, CommandOptions, CommandResult, CommandGroup,
        DispatchError, StandardFramework,
        macros::{command, group}
    },
    prelude::*,
    model::{
        channel::{Channel, Message}
    }
};
use serenity::http::Http;

use crate::DatabasePool;

#[command]
async fn rank(ctx: &Context, msg: &Message) -> CommandResult {
    let guild = msg.guild_id.unwrap();
    let data = ctx.data.read().await;
    let pool = data.get::<DatabasePool>().unwrap();
    let colour = Colour::from_rgb(255, 0, 0);
    let user = sqlx::query!("SELECT author FROM levels WHERE guild = $1", guild.0 as i64).fetch_optional(pool).await.unwrap();

    if let Some(user) = user {
        let curr_xp = sqlx::query!("SELECT xp FROM levels WHERE guild = $1 AND author = $2", guild.0 as i64, msg.author.id.0 as i64).fetch_optional(pool).await.unwrap();
        let level = sqlx::query!("SELECT lvl FROM levels WHERE guild = $1 AND author = $2", guild.0 as i64, msg.author.id.0 as i64).fetch_optional(pool).await.unwrap();
        let current_xp = curr_xp.unwrap().xp.unwrap();
        let current_lvl = level.unwrap().lvl.unwrap();

        if let Err(why) = msg.channel_id.send_message(&ctx.http, |m| {
            m.embed(|e|{
                e.title("Rank Card");
                e.description(format!("Heres your rank {}!", msg.author.name));
                e.colour(colour);
                e.field("**Level**", &current_lvl.to_string(), true);
                e.field("**XP**", &current_xp.to_string(), false);
                e
            });
            m
        }).await {
            eprintln!("Error regarding rank command | {:?}", why);
        }
    } else {
        msg.channel_id.say(ctx, "That user isnt ranked yet!").await?;
    }

    Ok(())
}

#[command]
#[required_permissions(MANAGE_MESSAGES)]
async fn disable(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;
    let pool = data.get::<DatabasePool>().unwrap();
    let guild = msg.guild_id.unwrap();

    let stat = sqlx::query!("SELECT status FROM levels WHERE guild = $1 AND author = $2", guild.0 as i64, msg.author.id.0 as i64).fetch_optional(pool).await?;

    let stat_bool = stat.unwrap().status.unwrap();

    if stat_bool == false {
        msg.channel_id.say(ctx, "Leveling is already disabled!").await?;
    } else {
        sqlx::query!("UPDATE levels SET status = $1 WHERE guild = $2", false, guild.0 as i64).execute(pool).await?;
        msg.channel_id.say(ctx, "I have disabled leveling in this server!").await?;
    }

    Ok(())
}

#[command]
#[required_permissions(MANAGE_MESSAGES)]
async fn enable(ctx: &Context, msg: &Message) -> CommandResult {
    let guild = msg.guild_id.unwrap();
    let author = msg.author.id.0 as i64;
    let data = ctx.data.read().await;
    let pool = data.get::<DatabasePool>().unwrap();
    let guild = msg.guild_id.unwrap();

    let stat = sqlx::query!("SELECT status FROM levels WHERE guild = $1 AND author = $2", guild.0 as i64,  msg.author.id.0 as i64).fetch_optional(pool).await?;

    let stat_bool = stat.unwrap().status.unwrap();

    if stat_bool == true {
        msg.channel_id.say(ctx, "Leveling is already enabled!").await?;
    } else {
        sqlx::query!("UPDATE levels SET status = $1 WHERE guild = $2", true, guild.0 as i64).execute(pool).await?;
        msg.channel_id.say(ctx, "I have reenabled leveling in this server!").await?;
    }

    Ok(())
}

#[command]
async fn leaderboard(ctx: &Context, msg: &Message) -> CommandResult {
    let guild = msg.guild_id.unwrap();
    let data = ctx.data.read().await;
    let pool = data.get::<DatabasePool>().unwrap();

    let results = sqlx::query!("SELECT author, lvl, xp FROM levels WHERE guild = $1 ORDER BY xp LIMIT 10", guild.0 as i64).fetch_all(pool).await?;

    for i in results {
        msg.channel_id.say(ctx, format!("{:?} | {} | {}", i.author.unwrap(), i.lvl.unwrap(), i.xp.unwrap())).await?;
    }

    Ok(())
}

#[command]
#[required_permissions(MANAGE_MESSAGES)]
async fn xprate(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let xp_rate = args.single::<i64>()?;
    if xp_rate < 1 || xp_rate > 1000 {
        msg.channel_id.say(ctx, "You can only set the xp rate between 1-1000").await?;
    } else {
        let guild = msg.guild_id.unwrap();
        let data = ctx.data.read().await;
        let pool = data.get::<DatabasePool>().unwrap();

        sqlx::query!("UPDATE guild_config SET xp_rate = $1 WHERE guild = $2", xp_rate, guild.0 as i64).execute(pool).await?;

        msg.channel_id.say(ctx, format!("I have set the xp rate to {}!", xp_rate)).await?;
    }

    Ok(())
}

#[command]
#[required_permissions(BAN_MEMBERS)]
async fn prefix(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let prefix = args.single::<String>()?;
    let data = ctx.data.read().await;
    let pool = data.get::<DatabasePool>().unwrap();
    let guild = &msg.guild_id.unwrap();

    sqlx::query!("UPDATE guild_config SET prefix = $1 WHERE guild = $2", prefix, guild.0 as i64).execute(pool).await?;

    msg.channel_id.say(ctx, format!("I have set the prefix to {}!", &prefix)).await?;

    Ok(())
}
