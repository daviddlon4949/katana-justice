use serenity::{
    async_trait,
    utils::*,
    framework::standard::{
        Args, CheckResult, CommandOptions, CommandResult, CommandGroup,
        DispatchError, StandardFramework,
        macros::{command, group}
    },
    prelude::*,
    model::{
        channel::{Channel, Message}
    }
};

use std::env;

use sqlx;
use sqlx::postgres::PgPool;
use sqlx::postgres::PgConnection;
use sqlx::Connect;

use crate::DatabasePool; //Use our one connection lol

#[command]
#[sub_commands(create, delete, edit)]
async fn tag(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let title = args.message(); //grabs the first arg and makes it the title
    let data = ctx.data.read().await; //we read the data
    let pool = data.get::<DatabasePool>().unwrap(); //we then get the database pool

    let guild_id = msg.guild_id.unwrap();

    let tag = sqlx::query!("SELECT content FROM tags WHERE guild = $1 AND name = $2", guild_id.0 as i64, &title).fetch_optional(pool).boxed().await; //Get the tag they want heheheh

    match tag {
        Ok(t) => msg.channel_id.say(ctx, t.unwrap().content.unwrap()).await?,
        Err(why) => msg.channel_id.say(ctx, format!("Something went horribly wrong | {}", why)).await?,
    };

    Ok(())
}

#[command]
async fn create(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let title = args.single::<String>()?;
    let content = args.remains().unwrap(); //unwrap as this returns an option

    let guild_id = msg.guild_id.unwrap();

    let data = ctx.data.read().await; //read the context data and await
    let pool = data.get::<DatabasePool>().unwrap(); //get the data from the DatabasePool and unwrap it

    sqlx::query!("INSERT INTO tags(author, content, name, guild) VALUES ($1, $2, $3, $4)", msg.author.id.0 as i64, &content, &title, guild_id.0 as i64).execute(pool).await?; //commit and execute this query

    msg.channel_id.say(ctx, format!("I have created the tag {}", title)).await?; //he created it

    Ok(())
}

#[command]
async fn delete(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let title = args.single::<String>()?; //grab the first arg

    let guild_id = msg.guild_id.unwrap(); //unwrap the guild as it is an option

    let data = ctx.data.read().await; //read the context data and await
    let pool = data.get::<DatabasePool>().unwrap(); //get the data from the DatabasePool and unwrap it

    sqlx::query!("DELETE FROM tags WHERE name = $1 AND guild = $2", &title, guild_id.0 as i64).execute(pool).await?; //Execute this query to delete the tag

    msg.channel_id.say(ctx, format!("I have deleted the tag {}!", title)).await?;

    Ok(())
}

#[command]
async fn edit(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let title = args.single::<String>()?; //This is to know which tag to update lol
    let content = args.remains().unwrap(); //Grab all the message's arguments

    let guild_id = msg.guild_id.unwrap(); //Unwrap as it is an option
    let author_id = msg.author.id.0 as i64;

    let data = ctx.data.read().await; 
    let pool = data.get::<DatabasePool>().unwrap();

    sqlx::query!("UPDATE tags SET content = $1 WHERE name = $2 AND author = $3", &content, &title, author_id).execute(pool).await?; //This query updates the table tags and sets content to the updated content

    msg.channel_id.say(ctx, format!("I have updated the tag {}!", title)).await?;

    Ok(())
}
