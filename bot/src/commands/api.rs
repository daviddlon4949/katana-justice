use serenity::{
    async_trait,
    framework::standard::{
        Args, CheckResult, CommandOptions, CommandResult, CommandGroup,
        DispatchError, StandardFramework,
        macros::{command, group}
    },
    prelude::*,
    model::{
        channel::{Channel, Message}
    }
};
use serenity::http::Http;

use rand::Rng;

use serde::Deserialize;
use serde_json;

use std::env;
 //to trace bojects (I have a bad time of doing this shit)

//reqwest to well... make requests!
use reqwest::{
    Client as Rclient,
    header::*,
    Url as urlr,
};
#[derive(Debug, Deserialize)]
pub struct InitalizDt {
    pub data: Data,
}

#[derive(Debug, Deserialize)]
pub struct Data {
    pub platformInfo: PlatformInfo,
    pub userInfo: InfoAbout,
    pub metadata: MetaData,
    pub segments: Vec<Segments>,
}

#[derive(Debug, Deserialize)]
pub struct PlatformInfo {
    pub platformSlug: String,
    pub platformUserId: String,
    pub platformUserIdentifier: String,
    pub avatarUrl: String,
}

#[derive(Debug, Deserialize)]
pub struct InfoAbout {
    pub isPremium: String,
    pub isVerified: String,
    pub isInfluencer: String,
    pub countryCode: String,
    pub socialAccounts: Vec<AccountsForUser>,
}

#[derive(Debug, Deserialize)]
pub struct AccountsForUser {
    pub platformSlug: String,
    pub platformUserIdentifier: String,
}

#[derive(Debug, Deserialize)]
pub struct MetaData {
    pub activeLegendName: String
}

#[derive(Debug, Deserialize)]
pub struct Segments {
    pub metadata: MetaMetaData,
    pub stats: Stats,
}

#[derive(Debug, Deserialize)]
pub struct MetaMetaData {
    pub name: String
}

#[derive(Debug, Deserialize)]
pub struct Stats {
    pub level: Level,
    pub kills: Kills,
}

#[derive(Debug, Deserialize)]
pub struct Level {
    pub rank: u32,
    pub value: u32,
}

#[derive(Debug, Deserialize)]
pub struct Kills {
    pub rank: u32,
    pub value: u32,
}

/* Credit to nitsuga: https://gitlab.com/nitsuga5124 for whats below lol*/
#[derive(Debug, Deserialize)]
pub struct Welcome {
    pub kind: String,
    pub data: WelcomeData,
}

#[derive(Debug, Deserialize)]
pub struct WelcomeData {
    pub modhash: String,
    pub dist: i64,
    pub children: Vec<Child>,
    pub after: String,
    pub before: Option<serde_json::Value>,
}

#[derive(Debug, Deserialize)]
pub struct Child {
    pub kind: Kind,
    pub data: ChildData,
}

#[derive(Debug, Deserialize)]
pub struct ChildData {
    pub approved_at_utc: Option<serde_json::Value>,
    pub subreddit: Subreddit,
    pub selftext: String,
    pub author_fullname: String,
    pub saved: bool,
    pub mod_reason_title: Option<serde_json::Value>,
    pub gilded: i64,
    pub clicked: bool,
    pub title: String,
    pub link_flair_richtext: Vec<Option<serde_json::Value>>,
    pub subreddit_name_prefixed: SubredditNamePrefixed,
    pub hidden: bool,
    pub pwls: i64,
    pub link_flair_css_class: Option<serde_json::Value>,
    pub downs: i64,
    pub thumbnail_height: Option<i64>,
    pub hide_score: bool,
    pub name: String,
    pub quarantine: bool,
    pub link_flair_text_color: LinkFlairTextColor,
    pub upvote_ratio: f64,
    pub author_flair_background_color: Option<serde_json::Value>,
    pub subreddit_type: SubredditType,
    pub ups: i64,
    pub total_awards_received: i64,
    pub media_embed: MediaEmbed,
    pub thumbnail_width: Option<i64>,
    pub author_flair_template_id: Option<serde_json::Value>,
    pub is_original_content: bool,
    pub user_reports: Vec<Option<serde_json::Value>>,
    pub secure_media: Option<Media>,
    pub is_reddit_media_domain: bool,
    pub is_meta: bool,
    pub category: Option<serde_json::Value>,
    pub secure_media_embed: MediaEmbed,
    pub link_flair_text: Option<serde_json::Value>,
    pub can_mod_post: bool,
    pub score: i64,
    pub approved_by: Option<serde_json::Value>,
    pub author_premium: bool,
    pub thumbnail: String,
    pub edited: Edited,
    pub author_flair_css_class: Option<serde_json::Value>,
    pub author_flair_richtext: Vec<Option<serde_json::Value>>,
    pub gildings: Gildings,
    pub post_hint: Option<PostHint>,
    pub content_categories: Option<serde_json::Value>,
    pub is_self: bool,
    pub mod_note: Option<serde_json::Value>,
    pub created: i64,
    pub link_flair_type: FlairType,
    pub wls: i64,
    pub removed_by_category: Option<serde_json::Value>,
    pub banned_by: Option<serde_json::Value>,
    pub author_flair_type: FlairType,
    pub domain: Domain,
    pub allow_live_comments: bool,
    pub selftext_html: Option<String>,
    pub likes: Option<serde_json::Value>,
    pub suggested_sort: Option<serde_json::Value>,
    pub banned_at_utc: Option<serde_json::Value>,
    pub view_count: Option<serde_json::Value>,
    pub archived: bool,
    pub no_follow: bool,
    pub is_crosspostable: bool,
    pub pinned: bool,
    pub over_18: bool,
    pub preview: Preview,
    pub all_awardings: Vec<Option<serde_json::Value>>,
    pub awarders: Vec<Option<serde_json::Value>>,
    pub media_only: bool,
    pub can_gild: bool,
    pub spoiler: bool,
    pub locked: bool,
    pub author_flair_text: Option<serde_json::Value>,
    pub treatment_tags: Vec<Option<serde_json::Value>>,
    pub visited: bool,
    pub removed_by: Option<serde_json::Value>,
    pub num_reports: Option<serde_json::Value>,
    pub distinguished: Option<String>,
    pub subreddit_id: SubredditId,
    pub mod_reason_by: Option<serde_json::Value>,
    pub removal_reason: Option<serde_json::Value>,
    pub link_flair_background_color: String,
    pub id: String,
    pub is_robot_indexable: bool,
    pub report_reasons: Option<serde_json::Value>,
    pub author: String,
    pub discussion_type: Option<serde_json::Value>,
    pub num_comments: i64,
    pub send_replies: bool,
    pub whitelist_status: WhitelistStatus,
    pub contest_mode: bool,
    pub mod_reports: Vec<Option<serde_json::Value>>,
    pub author_patreon_flair: bool,
    pub author_flair_text_color: Option<serde_json::Value>,
    pub permalink: String,
    pub parent_whitelist_status: WhitelistStatus,
    pub stickied: bool,
    pub url: String,
    pub subreddit_subscribers: i64,
    pub created_utc: i64,
    pub num_crossposts: i64,
    pub media: Option<Media>,
    pub is_video: bool,
}

#[derive(Debug, Deserialize)]
pub struct Gildings {
}

#[derive(Debug, Deserialize)]
pub struct Media {
    #[serde(rename = "type")]
    pub media_type: Domain,
    pub oembed: Oembed,
}

#[derive(Debug, Deserialize)]
pub struct Oembed {
    pub provider_url: String,
    pub description: String,
    pub title: String,
    #[serde(rename = "type")]
    pub oembed_type: String,
    pub author_name: Option<String>,
    pub height: i64,
    pub width: i64,
    pub html: String,
    pub thumbnail_width: i64,
    pub version: String,
    pub provider_name: String,
    pub thumbnail_url: String,
    pub thumbnail_height: i64,
    pub url: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct MediaEmbed {
    pub content: Option<String>,
    pub width: Option<i64>,
    pub scrolling: Option<bool>,
    pub height: Option<i64>,
    pub media_domain_url: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct Preview {
    pub images: Vec<Image>,
    pub enabled: bool,
    pub reddit_video_preview: Option<RedditVideoPreview>,
}

#[derive(Debug, Deserialize)]
pub struct Image {
    pub source: Source,
    pub resolutions: Vec<Source>,
    pub variants: Variants,
    pub id: String,
}

#[derive(Debug, Deserialize)]
pub struct Source {
    pub url: String,
    pub width: i64,
    pub height: i64,
}

#[derive(Debug, Deserialize)]
pub struct Variants {
    pub obfuscated: Nsfw,
    pub nsfw: Nsfw,
    pub gif: Option<Nsfw>,
    pub mp4: Option<Nsfw>,
}

#[derive(Debug, Deserialize)]
pub struct Nsfw {
    pub source: Source,
    pub resolutions: Vec<Source>,
}

#[derive(Debug, Deserialize)]
pub struct RedditVideoPreview {
    pub fallback_url: String,
    pub height: i64,
    pub width: i64,
    pub scrubber_media_url: String,
    pub dash_url: String,
    pub duration: i64,
    pub hls_url: String,
    pub is_gif: bool,
    pub transcoding_status: String,
}

#[derive(Debug, Deserialize)]
#[serde(untagged)]
pub enum Edited {
    Bool(bool),
    Integer(i64),
}

#[derive(Debug, Deserialize)]
pub enum FlairType {
    #[serde(rename = "text")]
    Text,
}

#[derive(Debug, Deserialize)]
pub enum Domain {
    #[serde(rename = "gfycat.com")]
    GfycatCom,
    #[serde(rename = "imgur.com")]
    ImgurCom,
    #[serde(rename = "self.hentai")]
    SelfHentai,
}

#[derive(Debug, Deserialize)]
pub enum LinkFlairTextColor {
    #[serde(rename = "dark")]
    Dark,
}

#[derive(Debug, Deserialize)]
pub enum WhitelistStatus {
    #[serde(rename = "promo_adult_nsfw")]
    PromoAdultNsfw,
}

#[derive(Debug, Deserialize)]
pub enum PostHint {
    #[serde(rename = "image")]
    Image,
    #[serde(rename = "link")]
    Link,
    #[serde(rename = "self")]
    PostHintSelf,
    #[serde(rename = "rich:video")]
    RichVideo,
}

#[derive(Debug, Deserialize)]
pub enum Subreddit {
    #[serde(rename = "hentai")]
    Hentai,
}

#[derive(Debug, Deserialize)]
pub enum SubredditId {
    #[serde(rename = "t5_2qj7g")]
    T52Qj7G,
}

#[derive(Debug, Deserialize)]
pub enum SubredditNamePrefixed {
    #[serde(rename = "r/hentai")]
    RHentai,
}

#[derive(Debug, Deserialize)]
pub enum SubredditType {
    #[serde(rename = "public")]
    Public,
}

#[derive(Debug, Deserialize)]
pub enum Kind {
    #[serde(rename = "t3")]
    T3,
}

/* Meme Command
    Usage: k!meme
    This command has a lot of things involved, so uhh hang tight
    As of rn this command is kinda wrong*/
/* #[command]
async fn meme(ctx: &Context, msg: &Message) -> CommandResult {
    let url = "https://www.reddit.com/r/dankmemes/top.json?&limit=40";
    println!("e");

    event!(Level::INFO, "Triggering here if happens!");
    let request = Rclient::new();
    event!(Level::INFO, "Triggering here if something goes wrong");
    let resp = request.get(url)
        .send()
        .await?
        .json::<Welcome>()
        .await?;
    event!(Level::INFO, "Sir, something is wrong here!");


    let rng = rand::thread_rng().gen_range(0, &resp.data.children.len());
    let information = &resp.data.children[rng].data;
    eprintln!("{:?}", information);
    let preview = &information.preview;
    let parsed_image = &preview.images[0].source.url.split('?').nth(0).unwrap().replace("preview", "i");
    eprintln!("{}", parsed_image);

    if let Err(why) = msg.channel_id.send_message(&ctx.http, |m|{
        m.embed(|e|{
            e.title(&information.title);
            e.description(&information.permalink);
            e.image(&parsed_image.to_string());
            e
        });
        m
    }).await {
        eprintln!("Error regarding meme | {:?}", why);
    }

    Ok(())
}

#[command]
async fn apex(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    kankyo::load().expect("Failed to load env file!"); //with kankyo, we can load our .env file
    let api_token = env::var("TRACKER_API_KEY").expect("Expected the key!"); //Load our Tracker Key: Developer Apps: https://tracker.gg/developers/docs/getting-started

    let platform = args.single::<String>()?; //Parse one arg as a String
    let identifier = args.single::<String>()?; //Same
    let url = format!("https://public-api.tracker.gg/v2/apex/standard/profile/{}/{}", platform, identifier); //Format a url with both of the insertions

    let request = Rclient::new(); //Make a new reqwest client
    let response = request.get(&url)
        .header("TRN-Api-Key", api_token) //The API requires to put our api token as a header
        .send() //send the request
        .await? //await
        .json::<InitalizDt>() //send the JSON to a struct
        .await?;

    if let Err(why) = msg.channel_id.send_message(&ctx.http, |m|{ //Ahh yes making embeds, lambdas are pretty cool in Rust
        m.embed(|e|{
            e.title("Apex Legends Stats");
            e.description(format!("Here are the stats about {}", &response.data.platformInfo.platformUserId));
            e.image(&response.data.platformInfo.avatarUrl);
            e
        });
        m
    }).await {
        eprintln!("Error regarding Apex Command | {:?}", why);
    }

    Ok(())
} */
